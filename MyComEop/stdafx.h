// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//







#pragma once

#include "targetver.h"
#include <comdef.h>
#include <ComSvcs.h>
#include <winternl.h>

typedef LUID OXID;
typedef LUID OID;
typedef GUID	IPID;

typedef struct tagSTDOBJREF    {
	DWORD   flags;
	DWORD   cPublicRefs;
	OXID           oxid;
	OID            oid;
	IPID           ipid;
} STDOBJREF;


typedef struct tagDUALSTRINGARRAY    {
	unsigned short wNumEntries;     // Number of entries in array.
	unsigned short wSecurityOffset; // Offset of security info.
	unsigned short aStringArray[1];
} DUALSTRINGARRAY;


typedef struct tagOBJREF    {
	unsigned long signature;//MEOW
	unsigned long flags;
	GUID          iid;
	byte     data[44];
} OBJREF;

struct  CContext 
{
	unsigned int ctxid;
	unsigned int pctxNext;
};
typedef struct _GLOBAL_SHARED_CRITICAL_SECTION
{
	int LockCount;
	int RecursionCount;
	unsigned int OwningThread;
}GLOBAL_SHARED_CRITICAL_SECTION;

struct  CDfMutex
{
	_GLOBAL_SHARED_CRITICAL_SECTION *_pGlobalPortion;
	void *_hLockEvent;
};





typedef struct  CContextList
{
	unsigned int _pctxHead;
	int _cReferences;
}_CContextList;





typedef struct  _CGlobalContext
{
	CContextList CContextListbase;
	int _fTakeLock;
	unsigned int _dfOpenLock;
	IMalloc *const MallocObjPtr;
	const int _fCreated;
	LARGE_INTEGER _luidMutexName;
	GLOBAL_SHARED_CRITICAL_SECTION _GlobalPortion;
} CGlobalContext;

struct  CPerContext
{
	CContext baseclass_CContext;
	ILockBytes *_LockBytesBasePtr;
	void *_CFileStreamDirtyPtr;
	ILockBytes *_LockBytesOriginalPtr;
	unsigned int _ulOpenLock;
	unsigned int CGlobalContextPtr;
	int _CountReferences;
	IMalloc *const _pMalloc;
	void *_pfi;
	void *_hNotificationEvent;
	void *_psmb;
	char *_pbBase;
	unsigned int _ulHeapName;
	CDfMutex _dmtx;
	unsigned int _cRecursion;
};
struct __declspec(align(4)) CBasedPubDocFilePtr
{
	unsigned int _SelftobjectPtr;
};

struct __declspec(align(4)) CBasedMapSectionPtr
{
	unsigned int _SelftobjectPtr;
};
struct  CGlobalFileStream 
{
	CContextList baseclass_CContextList;
	unsigned int _dfFlagFirst;
	unsigned int _dwStartFlags;
	IMalloc *const _pMalloc;
	unsigned __int64 _ulPos;
	unsigned int _cbSector;
	unsigned int _cbMappedFileSize;
	unsigned int _cbMappedCommitSize;
	unsigned int _dwMapFlags;
	wchar_t _awcPath[261];
	unsigned int _dwTerminate;
	unsigned __int64 _ulHighWater;
	unsigned __int64 _ulFailurePoint;
};


struct  CFileStream
{
	unsigned int _ILockBytesField;
	IUnknown* IFileLockBytesObj;
	IUnknown* baseclass_8;
	IUnknown* baseclass_c;
	CContext baseContext;
	unsigned int _CGlobalFileStreamPtr;
	CPerContext *_ppc;
	HANDLE _hFile;
	HANDLE _hPreDuped;
	unsigned int _sig;
	int _cReferences;
	IMalloc *const _pMalloc;
	void *_hMapObject;
	char *_pbBaseAddr;
	unsigned int _cbViewSize;
};


typedef  struct _SDfMarshalPacket
{
	unsigned int CBasedPubDocFileObj;
	void *  CBasedPubStreamObj;
	void *  CBasedSeekPointerObj;
	void *  CBasedMarshalListObj;
	void *  CBasedDFBasisObj;
	unsigned int CBasedGlobalContextObj;
	unsigned int CBasedGlobalFileStreamBaseObj;
	void *  CBasedGlobalFileStreamDirty;
	void *  CBasedGlobalFileStreamOriginal;
	unsigned int ulHeapName;
	unsigned int ProcessContextId;
	GUID cntxkey;
	CPerContext * CPerContextObj;
	void *hMem;
} SDfMarshalPacket;

#define STATUS_INFO_LENGTH_MISMATCH 0xc0000004

 enum SystemInformationClass
{
	SystemBasicInformation0,
	SystemProcessorInformation,
	SystemPerformanceInformation0,
	SystemTimeOfDayInformation0,
	SystemPathInformation,
	SystemProcessInformation0,
	SystemCallCountInformation,
	SystemDeviceInformation,
	SystemProcessorPerformanceInformation0,
	SystemFlagsInformation,
	SystemCallTimeInformation,
	SystemModuleInformation,
	SystemLocksInformation,
	SystemStackTraceInformation,
	SystemPagedPoolInformation,
	SystemNonPagedPoolInformation,
	SystemHandleInformation,
	SystemObjectInformation,
	SystemPageFileInformation,
	SystemVdmInstemulInformation,
	SystemVdmBopInformation,
	SystemFileCacheInformation,
	SystemPoolTagInformation,
	SystemInterruptInformation0,
	SystemDpcBehaviorInformation,
	SystemFullMemoryInformation,
	SystemLoadGdiDriverInformation,
	SystemUnloadGdiDriverInformation,
	SystemTimeAdjustmentInformation,
	SystemSummaryMemoryInformation,
	SystemMirrorMemoryInformation,
	SystemPerformanceTraceInformation,
	SystemObsolete0,
	SystemExceptionInformation0,
	SystemCrashDumpStateInformation,
	SystemKernelDebuggerInformation,
	SystemContextSwitchInformation,
	SystemRegistryQuotaInformation0,
	SystemExtendServiceTableInformation,
	SystemPrioritySeperation,
	SystemVerifierAddDriverInformation,
	SystemVerifierRemoveDriverInformation,
	SystemProcessorIdleInformation,
	SystemLegacyDriverInformation,
	SystemCurrentTimeZoneInformation,
	SystemLookasideInformation0,
	SystemTimeSlipNotification,
	SystemSessionCreate,
	SystemSessionDetach,
	SystemSessionInformation,
	SystemRangeStartInformation,
	SystemVerifierInformation,
	SystemVerifierThunkExtend,
	SystemSessionProcessInformation,
	SystemLoadGdiDriverInSystemSpace,
	SystemNumaProcessorMap,
	SystemPrefetcherInformation,
	SystemExtendedProcessInformation,
	SystemRecommendedSharedDataAlignment,
	SystemComPlusPackage,
	SystemNumaAvailableMemory,
	SystemProcessorPowerInformation,
	SystemEmulationBasicInformation,
	SystemEmulationProcessorInformation,
	SystemExtendedHandleInformation,
	SystemLostDelayedWriteInformation,
	SystemBigPoolInformation,
	SystemSessionPoolTagInformation,
	SystemSessionMappedViewInformation,
	SystemHotpatchInformation,
	SystemObjectSecurityMode,
	SystemWatchdogTimerHandler,
	SystemWatchdogTimerInformation,
	SystemLogicalProcessorInformation,
	SystemWow64SharedInformationObsolete,
	SystemRegisterFirmwareTableInformationHandler,
	SystemFirmwareTableInformation,
	SystemModuleInformationEx,
	SystemVerifierTriageInformation,
	SystemSuperfetchInformation,
	SystemMemoryListInformation,
	SystemFileCacheInformationEx,
	SystemThreadPriorityClientIdInformation,
	SystemProcessorIdleCycleTimeInformation,
	SystemVerifierCancellationInformation,
	SystemProcessorPowerInformationEx,
	SystemRefTraceInformation,
	SystemSpecialPoolInformation,
	SystemProcessIdInformation,
	SystemErrorPortInformation,
	SystemBootEnvironmentInformation,
	SystemHypervisorInformation,
	SystemVerifierInformationEx,
	SystemTimeZoneInformation,
	SystemImageFileExecutionOptionsInformation,
	SystemCoverageInformation,
	SystemPrefetchPatchInformation,
	SystemVerifierFaultsInformation,
	SystemSystemPartitionInformation,
	SystemSystemDiskInformation,
	SystemProcessorPerformanceDistribution,
	SystemNumaProximityNodeInformation,
	SystemDynamicTimeZoneInformation,
	SystemCodeIntegrityInformation,
	SystemProcessorMicrocodeUpdateInformation,
	SystemProcessorBrandString,
	SystemVirtualAddressInformation,
	SystemLogicalProcessorAndGroupInformation,
	SystemProcessorCycleTimeInformation,
	SystemStoreInformation,
	SystemRegistryAppendString,
	SystemAitSamplingValue,
	SystemVhdBootInformation,
	SystemCpuQuotaInformation,
	SystemNativeBasicInformation,
	SystemErrorPortTimeouts,
	SystemLowPriorityIoInformation,
	SystemBootEntropyInformation,
	SystemVerifierCountersInformation,
	SystemPagedPoolInformationEx,
	SystemSystemPtesInformationEx,
	SystemNodeDistanceInformation,
	SystemAcpiAuditInformation,
	SystemBasicPerformanceInformation,
	SystemQueryPerformanceCounterInformation,
	SystemSessionBigPoolInformation,
	SystemBootGraphicsInformation,
	SystemScrubPhysicalMemoryInformation,
	SystemBadPageInformation,
	SystemProcessorProfileControlArea,
	SystemCombinePhysicalMemoryInformation,
	SystemEntropyInterruptTimingInformation,
	SystemConsoleInformation,
	SystemPlatformBinaryInformation,
	SystemPolicyInformation0,
	SystemHypervisorProcessorCountInformation,
	SystemDeviceDataInformation,
	SystemDeviceDataEnumerationInformation,
	SystemMemoryTopologyInformation,
	SystemMemoryChannelInformation,
	SystemBootLogoInformation,
	SystemProcessorPerformanceInformationEx,
	SystemCriticalProcessErrorLogInformation,
	SystemSecureBootPolicyInformation,
	SystemPageFileInformationEx,
	SystemSecureBootInformation,
	SystemEntropyInterruptTimingRawInformation,
	SystemPortableWorkspaceEfiLauncherInformation,
	SystemFullProcessInformation,
	SystemKernelDebuggerInformationEx,
	SystemBootMetadataInformation,
	SystemSoftRebootInformation,
	SystemElamCertificateInformation,
	SystemOfflineDumpConfigInformation,
	SystemProcessorFeaturesInformation,
	SystemRegistryReconciliationInformation,
	SystemEdidInformation,
	SystemManufacturingInformation,
	SystemEnergyEstimationConfigInformation,
	SystemHypervisorDetailInformation,
	SystemProcessorCycleStatsInformation,
	SystemVmGenerationCountInformation,
	SystemTrustedPlatformModuleInformation,
	SystemKernelDebuggerFlags,
	SystemCodeIntegrityPolicyInformation,
	SystemIsolatedUserModeInformation,
	SystemHardwareSecurityTestInterfaceResultsInformation,
	SystemSingleModuleInformation,
	SystemAllowedCpuSetsInformation,
	SystemVsmProtectionInformation,
	SystemInterruptCpuSetsInformation,
	SystemSecureBootPolicyFullInformation,
	SystemCodeIntegrityPolicyFullInformation,
	SystemAffinitizedInterruptProcessorInformation,
	SystemRootSiloInformation,
	SystemCpuSetInformation,
	SystemCpuSetTagInformation,
	SystemWin32WerStartCallout,
	SystemSecureKernelProfileInformation,
	SystemCodeIntegrityPlatformManifestInformation,
	SystemInterruptSteeringInformation,
	SystemSupportedProcessorArchitectures,
	SystemMemoryUsageInformation,
	SystemCodeIntegrityCertificateInformation,
	SystemPhysicalMemoryInformation,
	SystemControlFlowTransition,
	SystemKernelDebuggingAllowed,
	SystemActivityModerationExeState,
	SystemActivityModerationUserSettings,
	SystemCodeIntegrityPoliciesFullInformation,
	SystemCodeIntegrityUnlockInformation,
	SystemIntegrityQuotaInformation,
	SystemFlushInformation,
	SystemProcessorIdleMaskInformation,
	SystemSecureDumpEncryptionInformation,
	SystemWriteConstraintInformation,
	SystemKernelVaShadowInformation,
	SystemHypervisorSharedPageInformation,
	SystemFirmwareBootPerformanceInformation,
	SystemCodeIntegrityVerificationInformation,
	SystemFirmwarePartitionInformation,
	SystemSpeculationControlInformation,
	SystemDmaGuardPolicyInformation,
	SystemEnclaveLaunchControlInformation,
	MaxSystemInfoClass,
};

typedef  struct _SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX {
	PVOID* Object;
	ULONG_PTR UniqueProcessId;
	HANDLE HandleValue;
	ACCESS_MASK GrantedAccess;
	USHORT CreatorBackTraceIndex;
	USHORT ObjectTypeIndex;
	ULONG HandleAttributes;
	ULONG Reserved;
}SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX;

typedef  struct _SYSTEM_HANDLE_INFORMATION_EX {
	ULONG_PTR NumberOfHandles;
	ULONG_PTR Reserved;
	SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX Handles[1];
} SYSTEM_HANDLE_INFORMATION_EX;

typedef enum _POOL_TYPE
{
	NonPagedPool,
	PagedPool,
	NonPagedPoolMustSucceed,
	DontUseThisType,
	NonPagedPoolCacheAligned,
	PagedPoolCacheAligned,
	NonPagedPoolCacheAlignedMustS
} POOL_TYPE, *PPOOL_TYPE;

typedef struct _OBJECT_TYPE_INFORMATION
{
	UNICODE_STRING  Name;
	
} OBJECT_TYPE_INFORMATION, *POBJECT_TYPE_INFORMATION;

typedef struct _OBJECT_TYPES_INFORMATION
{
	ULONG NumberOfTypes;
	OBJECT_TYPE_INFORMATION TypeInformation[1];
} OBJECT_TYPES_INFORMATION, *POBJECT_TYPES_INFORMATION;

enum NewObjectInformationClass
{
	ObjectBasicInformation0,
	ObjectNameInformation,
	ObjectTypeInformation0,
	ObjectTypesInformation,
	ObjectHandleFlagInformation,
	ObjectSessionInformation,
	ObjectSessionObjectInformation,
};


typedef enum _SECTION_INFORMATION_CLASS {
	SectionBasicInformation,
	SectionImageInformation
} SECTION_INFORMATION_CLASS;


typedef enum _MEMORY_INFORMATION_CLASS
{
	MemoryBasicInformation,
	MemoryWorkingSetList,
	MemorySectionName,
	MemoryBasicVlmInformation
} MEMORY_INFORMATION_CLASS;



typedef struct _SECTION_BASIC_INFORMATION {
	PVOID Base;
	ULONG Attributes;
	LARGE_INTEGER Size;
} SECTION_BASIC_INFORMATION;



typedef  enum _EVENT_TYPE
{
	NotificationEvent,
	SynchronizationEvent,
} EVENT_TYPE;

typedef enum _AttributeFlags
{
	/// <summary>None</summary>
	None = 0,
	/// <summary>The handle created can be inherited</summary>
	Inherit = 2,
	/// <summary>The object created is marked as permanent</summary>
	Permanent = 16, // 0x00000010
	/// <summary>The object must be created exclusively</summary>
	Exclusive = 32, // 0x00000020
	/// <summary>
	/// The object name lookup should be done case insensitive
	/// </summary>
	CaseInsensitive = 64, // 0x00000040
	/// <summary>Open the object if it already exists</summary>
	OpenIf = 128, // 0x00000080
	/// <summary>Open the object as a link</summary>
	OpenLink = 256, // 0x00000100
	/// <summary>Create as a kernel handle (not used in user-mode)</summary>
	KernelHandle = 512, // 0x00000200
	/// <summary>
	/// Force an access check to occur (not used in user-mode)
	/// </summary>
	ForceAccessCheck = 1024, // 0x00000400
	/// <summary>Ignore impersonated device map when looking up object</summary>
	IgnoreImpersonatedDevicemap = 2048, // 0x00000800
	/// <summary>Fail if a reparse is encountered</summary>
	DontReparse = 4096, // 0x00001000
}AttributeFlags;